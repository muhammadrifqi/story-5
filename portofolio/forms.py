from django import forms

from .models import Jadwal


class JadwalForm(forms.ModelForm):

    class Meta:
        model = Jadwal
        fields = ['tanggal', 'nama_Kegiatan', 'tempat', 'kategori']
        widgets = {
            'tanggal': forms.DateTimeInput(attrs={'class': 'datetime-input'})
        }
        
