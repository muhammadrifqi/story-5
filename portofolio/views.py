from django.shortcuts import render, get_object_or_404, redirect
from .models import Jadwal
from .forms import JadwalForm
from django.views.generic.edit import DeleteView
from django.urls import reverse_lazy

# Create your views here.
def index(request):
    return render(request, 'index.html')

def launch(request):
    return render(request, 'launch.html')

def about(request):
    return render(request, 'about.html')

def skills(request):
    return render(request, 'skills.html')

def experiences(request):
    return render(request, 'experiences.html')

def gallery(request):
    return render(request, 'gallery.html')

def contact(request):
    return render(request, 'contact.html')

def show_jadwal(request):
    semua_jadwal = Jadwal.objects.order_by('tanggal')
    return render(request, 'show_jadwal.html', {'semua_jadwal' : semua_jadwal})

def add_jadwal(request):
    if request.method == "POST":
        form = JadwalForm(request.POST)
        if form.is_valid():
            jadwal = form.save()
            jadwal.save()        
            return redirect('/schedules/')
    else:
        form = JadwalForm()
        
    return render(request, 'add_jadwal.html', {'form': form})

# def delete_jadwal(request, pk):
#     template = 'delete_jadwal.html'
#     kegiatan = get_object_or_404(Jadwal, pk=pk)

#     try:
#         if request.method == 'POST':
#             form = JadwalForm(request.POST, instance=kegiatan)
#             kegiatan.delete()
#             messages.success(request, 'Kegiatan berhasil dihapus')
#         else:
#             form = JadwalForm(instance=kegiatan)
#     context = {
#         'form':form,
#     }
#     return render(request, template, context)

class JadwalDelete(DeleteView):
    model = Jadwal
    success_url = reverse_lazy('portofolio:show_jadwal')


