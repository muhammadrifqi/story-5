from django.urls import path
from . import views
from django.conf.urls import url

app_name = 'portofolio'

urlpatterns = [
    path('', views.launch, name='launch'),
    path('about/', views.about, name='about'),
    path('skills/', views.skills, name='skills'),
    path('experiences/', views.experiences, name='experiences'),
    path('gallery/', views.gallery, name='gallery'),
    path('contact/', views.contact, name='contact'),
    path('schedules/', views.show_jadwal, name='show_jadwal'),
    path('schedules/add/', views.add_jadwal, name='add_jadwal'),

    url(r'schedules/(?P<pk>[0-9]+)/delete/$', views.JadwalDelete.as_view(), name='delete_jadwal'),
]
