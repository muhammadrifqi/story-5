from django.conf import settings
from django.db import models

# Create your models here.

class Jadwal(models.Model):
    tanggal = models.DateTimeField()
    nama_Kegiatan = models.CharField(max_length=100)
    tempat = models.CharField(max_length=50)
    kategori = models.CharField(max_length=50)

    def __str__(self):
        return self.nama_Kegiatan

    def getTanggal(self):
        return self.tanggal.strftime("%d")
    
    def getBulan(self):
        return self.tanggal.strftime("%b")

    def getHari(self):
        return ' ' + self.tanggal.strftime("%A")

    def getJam(self):
        return ' ' + self.tanggal.strftime("%H:%M:%S")

    def getTempat(self):
        return ' ' + self.tempat
    
